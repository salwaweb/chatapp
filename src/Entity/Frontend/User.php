<?php

namespace App\Entity\Frontend;

use Doctrine\ORM\Mapping as ORM;

/**
* User
*
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_957A647992FC23A8", columns={"username_canonical"}), @ORM\UniqueConstraint(name="UNIQ_957A6479C05FB297", columns={"confirmation_token"}), @ORM\UniqueConstraint(name="UNIQ_957A6479A0D96FBF", columns={"email_canonical"})}, indexes={@ORM\Index(name="caractere_id", columns={"caractere_id"}), @ORM\Index(name="etude_id", columns={"etude_id"}), @ORM\Index(name="profession_id", columns={"profession_id"}), @ORM\Index(name="situation_id", columns={"situation_id"}), @ORM\Index(name="abonnement_id", columns={"abonnement_id"}), @ORM\Index(name="gender_2", columns={"gender"}), @ORM\Index(name="cheveux_id", columns={"cheveux_id"}), @ORM\Index(name="fumeur_id", columns={"fumeur_id"}), @ORM\Index(name="religion_id", columns={"religion_id"}), @ORM\Index(name="pays", columns={"pays"}), @ORM\Index(name="gender", columns={"gender"}), @ORM\Index(name="alcool_id", columns={"alcool_id"})})
 * @ORM\Entity
    */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=180, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="username_canonical", type="string", length=180, nullable=false)
     */
    private $usernameCanonical;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=180, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="email_canonical", type="string", length=180, nullable=false)
     */
    private $emailCanonical;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * @var string|null
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="confirmation_token", type="string", length=180, nullable=true)
     */
    private $confirmationToken;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    private $passwordRequestedAt;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array", length=0, nullable=false)
     */
    private $roles;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prenom", type="string", length=100, nullable=true)
     */
    private $prenom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="lookingfor", type="integer", nullable=true)
     */
    private $lookingfor ;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=1024, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=100, nullable=true)
     */
    private $telephone;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var string|null
     *
     * @ORM\Column(name="zodiac", type="string", length=256, nullable=true)
     */
    private $zodiac;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adress", type="string", length=256, nullable=true)
     */
    private $adress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ville", type="string", length=256, nullable=true)
     */
    private $ville;

    /**
     * @var string|null
     *
     * @ORM\Column(name="longitude", type="string", length=256, nullable=true)
     */
    private $longitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="latitude", type="string", length=256, nullable=true)
     */
    private $latitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="zipdcode", type="string", length=256, nullable=true)
     */
    private $zipdcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=256, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="taille", type="string", length=256, nullable=true)
     */
    private $taille;

    /**
     * @var string|null
     *
     * @ORM\Column(name="poids", type="string", length=256, nullable=true)
     */
    private $poids;

    /**
     * @var string|null
     *
     * @ORM\Column(name="age", type="string", length=256, nullable=true)
     */
    private $age;

    /**
     * @var string|null
     *
     * @ORM\Column(name="distance", type="string", length=256, nullable=true)
     */
    private $distance;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gmail_id", type="string", length=256, nullable=true)
     */
    private $gmailId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="facebook_id", type="string", length=256, nullable=true)
     */
    private $facebookId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="solde", type="integer", nullable=true)
     */
    private $solde;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dateCreation = 'CURRENT_TIMESTAMP';

    /**
     * @var \Alcool
     *
     * @ORM\ManyToOne(targetEntity="Alcool")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="alcool_id", referencedColumnName="id")
     * })
     */
    private $alcool;

    /**
     * @var \Abonnement
     *
     * @ORM\ManyToOne(targetEntity="Abonnement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="abonnement_id", referencedColumnName="id")
     * })
     */
    private $abonnement;

    /**
     * @var \Gender
     *
     * @ORM\ManyToOne(targetEntity="Gender")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gender", referencedColumnName="id")
     * })
     */
    private $gender;

    /**
     * @var \Caractere
     *
     * @ORM\ManyToOne(targetEntity="Caractere")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="caractere_id", referencedColumnName="id")
     * })
     */
    private $caractere;

    /**
     * @var \Cheveux
     *
     * @ORM\ManyToOne(targetEntity="Cheveux")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cheveux_id", referencedColumnName="id")
     * })
     */
    private $cheveux;

    /**
     * @var \Etude
     *
     * @ORM\ManyToOne(targetEntity="Etude")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="etude_id", referencedColumnName="id")
     * })
     */
    private $etude;

    /**
     * @var \Fumer
     *
     * @ORM\ManyToOne(targetEntity="Fumer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fumeur_id", referencedColumnName="id")
     * })
     */
    private $fumeur;

    /**
     * @var \Profession
     *
     * @ORM\ManyToOne(targetEntity="Profession")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="profession_id", referencedColumnName="id")
     * })
     */
    private $profession;

    /**
     * @var \Religion
     *
     * @ORM\ManyToOne(targetEntity="Religion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="religion_id", referencedColumnName="id")
     * })
     */
    private $religion;

    /**
     * @var \Situation
     *
     * @ORM\ManyToOne(targetEntity="Situation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="situation_id", referencedColumnName="id")
     * })
     */
    private $situation;

    /**
     * @var \Pays
     *
     * @ORM\ManyToOne(targetEntity="Pays")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pays", referencedColumnName="id")
     * })
     */
    private $pays;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUsernameCanonical(): ?string
    {
        return $this->usernameCanonical;
    }

    public function setUsernameCanonical(string $usernameCanonical): self
    {
        $this->usernameCanonical = $usernameCanonical;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailCanonical(): ?string
    {
        return $this->emailCanonical;
    }

    public function setEmailCanonical(string $emailCanonical): self
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(?string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getPasswordRequestedAt(): ?\DateTimeInterface
    {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(?\DateTimeInterface $passwordRequestedAt): self
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getLookingfor(): ?int
    {
        return $this->lookingfor;
    }

    public function setLookingfor(?int $lookingfor): self
    {
        $this->lookingfor = $lookingfor;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getZodiac(): ?string
    {
        return $this->zodiac;
    }

    public function setZodiac(?string $zodiac): self
    {
        $this->zodiac = $zodiac;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getZipdcode(): ?string
    {
        return $this->zipdcode;
    }

    public function setZipdcode(?string $zipdcode): self
    {
        $this->zipdcode = $zipdcode;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(?string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getPoids(): ?string
    {
        return $this->poids;
    }

    public function setPoids(?string $poids): self
    {
        $this->poids = $poids;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(?string $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getDistance(): ?string
    {
        return $this->distance;
    }

    public function setDistance(?string $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getGmailId(): ?string
    {
        return $this->gmailId;
    }

    public function setGmailId(?string $gmailId): self
    {
        $this->gmailId = $gmailId;

        return $this;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getSolde(): ?int
    {
        return $this->solde;
    }

    public function setSolde(?int $solde): self
    {
        $this->solde = $solde;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getAlcool(): ?Alcool
    {
        return $this->alcool;
    }

    public function setAlcool(?Alcool $alcool): self
    {
        $this->alcool = $alcool;

        return $this;
    }

    public function getAbonnement(): ?Abonnement
    {
        return $this->abonnement;
    }

    public function setAbonnement(?Abonnement $abonnement): self
    {
        $this->abonnement = $abonnement;

        return $this;
    }

    public function getGender(): ?Gender
    {
        return $this->gender;
    }

    public function setGender(?Gender $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCaractere(): ?Caractere
    {
        return $this->caractere;
    }

    public function setCaractere(?Caractere $caractere): self
    {
        $this->caractere = $caractere;

        return $this;
    }

    public function getCheveux(): ?Cheveux
    {
        return $this->cheveux;
    }

    public function setCheveux(?Cheveux $cheveux): self
    {
        $this->cheveux = $cheveux;

        return $this;
    }

    public function getEtude(): ?Etude
    {
        return $this->etude;
    }

    public function setEtude(?Etude $etude): self
    {
        $this->etude = $etude;

        return $this;
    }

    public function getFumeur(): ?Fumer
    {
        return $this->fumeur;
    }

    public function setFumeur(?Fumer $fumeur): self
    {
        $this->fumeur = $fumeur;

        return $this;
    }

    public function getProfession(): ?Profession
    {
        return $this->profession;
    }

    public function setProfession(?Profession $profession): self
    {
        $this->profession = $profession;

        return $this;
    }

    public function getReligion(): ?Religion
    {
        return $this->religion;
    }

    public function setReligion(?Religion $religion): self
    {
        $this->religion = $religion;

        return $this;
    }

    public function getSituation(): ?Situation
    {
        return $this->situation;
    }

    public function setSituation(?Situation $situation): self
    {
        $this->situation = $situation;

        return $this;
    }

    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    public function setPays(?Pays $pays): self
    {
        $this->pays = $pays;

        return $this;
    }


}
