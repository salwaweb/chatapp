<?php

namespace App\Controller;

use App\Entity\CadeauxUser;
use App\Entity\Frontend\PaysUser;
use App\Entity\Frontend\UserMedia;
use App\Entity\LanguesUser;
use App\Entity\LoisirUser;
use App\Entity\SportUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/abonnement-unmois")
 */
class AbonnementUnmoisController extends AbstractController
{
    /**
     * @Route("/", name="abonnement_unmois")
     */
    public function index()
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $langues = $em->getRepository(LanguesUser::class)->findBy(array('user'=>$utilisateur));
        $loisirs = $em->getRepository(LoisirUser::class)->findBy(array('user'=>$utilisateur));
        $sports = $em->getRepository(SportUser::class)->findBy(array('user'=>$utilisateur));
        $upays = $em->getRepository(PaysUser::class)->findBy(array('user'=>$utilisateur));
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $cadeaux = $em->getRepository(CadeauxUser::class)->findBy(array('user'=>$utilisateur));
        return $this->render('abonnement_unmois/index.html.twig', [
            'controller_name' => 'AbonnementUnmoisController',
            'langues'=>$langues,
            'loisirs'=>$loisirs,
            'sports'=>$sports,
            'photos'=>$photos,
            'upays'=>$upays,
            'cadeaux'=>$cadeaux,
        ]);
    }
}
