<?php

namespace App\Controller;

use App\Entity\Cadeaux;
use App\Entity\CadeauxUser;
use App\Entity\Frontend\PaysUser;
use App\Entity\Frontend\User as FrontUser;
use App\Entity\Frontend\UserMedia;
use App\Entity\LanguesUser;
use App\Entity\Liked;
use App\Entity\LoisirUser;
use App\Entity\SportUser;
use App\Entity\User;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/abonnement-troismois")
 */
class AbonnementTroismoisController extends AbstractController
{

    /**
     * @Route("/", name="abonnement_troismois")
     */
    public function index()
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $langues = $em->getRepository(LanguesUser::class)->findBy(array('user'=>$utilisateur));
        $loisirs = $em->getRepository(LoisirUser::class)->findBy(array('user'=>$utilisateur));
        $sports = $em->getRepository(SportUser::class)->findBy(array('user'=>$utilisateur));
        $upays = $em->getRepository(PaysUser::class)->findBy(array('user'=>$utilisateur));
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $cadeaux = $em->getRepository(CadeauxUser::class)->findBy(array('user'=>$utilisateur));
        return $this->render('abonnement_troismois/index.html.twig', [
            'controller_name' => 'AbonnementTroismoisController',
            'langues'=>$langues,
            'loisirs'=>$loisirs,
            'sports'=>$sports,
            'photos'=>$photos,
            'upays'=>$upays,
            'cadeaux'=>$cadeaux,
        ]);
    }


    /**
     * @Route ("/rencontre", name="rencontres" )
     */
    public function rencontre(){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM App:User u WHERE u.roles NOT LIKE :role'
            )->setParameter('role', '%"ROLE_ADMIN"%');

        $users = $query->getResult();
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $medias = $em->getRepository(UserMedia::class)->findBy(array('defaultImg'=>1));
        $album = $em->getRepository(UserMedia::class)->findall();
        return $this->render('abonnement_troismois/rencontre2.html.twig',[
            'users'=>$users,
            'photos'=>$photos,
            'album'=>$album,
            'medias'=>$medias
        ]);

    }

    /**
     * @Route ("/connection", name="connection" )
     */
    public function connection(){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM App:User u WHERE u.roles NOT LIKE :role'
            )->setParameter('role', '%"ROLE_ADMIN"%');

        $users = $query->getResult();
       //return($users);
        dd($users);
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $medias = $em->getRepository(UserMedia::class)->findBy(array('defaultImg'=>1));

        return $this->render('abonnement_troismois/rencontre.html.twig',[
            'user'=>$users
        ]);
    }
    /**
     * @Route ("/{id}/liked", name="liked" )
     */
    public function liked(Request $request, User $id){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));

        $liked= new Liked();
        $liked->setUtilisateur($utilisateur);
        $liked->setLiked($userlike);
        $liked->setDatecreation(new \DateTime());
        $em->persist($liked);
        $em->flush();
        return $this->redirectToRoute('rencontres');
    }



    /**
     * @Route ("/like-from-you", name="like_from_you" )
     */
    public function likefrom(Request $request, PaginatorInterface $paginator){

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $liked = $em->getRepository(Liked::class)->findBy(array('utilisateur'=>$utilisateur));
        $likes= $paginator->paginate($liked, $request->query->getInt('page', 1), 2);
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $medias = $em->getRepository(UserMedia::class)->findBy(array('defaultImg'=>1));

        return $this->render('abonnement_troismois/like-from-you.html.twig',[
            'likes'=>$likes,
            'photos'=>$photos,
            'medias'=>$medias
        ]);
    }
    /**
     * @Route ("/{id}/dislike-from", name="dislike_from" )
     */
    public function dislikefrom(Request $request, User $id){

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $like= $em ->getRepository(Liked::class)->findOneBy(array('utilisateur'=>$utilisateur, 'liked'=>$userlike));
        $like->setStatus(0);
        $like->setDatecreation(new \DateTime());
        $em->persist($like);
        $em->flush();

        return $this->redirectToRoute('detailuser',array('id'=>$userlike->getId()));

    }
    /**
     * @Route ("/{id}/like-from", name="like_from" )
     */
    public function likedfrom(Request $request, User $id){

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $like= $em ->getRepository(Liked::class)->findOneBy(array('utilisateur'=>$utilisateur, 'liked'=>$userlike));
        $like->setStatus(1);
        $like->setDatecreation(new \DateTime());
        $em->persist($like);
        $em->flush();
        return $this->redirectToRoute('detailuser',array('id'=>$userlike->getId()));

    }
    /**
     * @Route("/{id}/detail-user", name="detailuser" )
     */
    public function detail(Request $request,User $id){
        $em= $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $user= $em->getRepository(FrontUser::class)->findOneBy(array( 'id'=>$id));
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$user));
        $langues = $em->getRepository(LanguesUser::class)->findBy(array('user'=>$utilisateur));
        $loisirs = $em->getRepository(LoisirUser::class)->findBy(array('user'=>$utilisateur));
        $sports = $em->getRepository(SportUser::class)->findBy(array('user'=>$utilisateur));
        $upays = $em->getRepository(PaysUser::class)->findBy(array('user'=>$utilisateur));
        $cadeaux = $em->getRepository(Cadeaux::class)->findAll();
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $like= $em ->getRepository(Liked::class)->findOneBy(array('utilisateur'=>$utilisateur, 'liked'=>$userlike));
        $medias=$em->getRepository(UserMedia::class)->findBy(['user'=>$user,'status'=>1]);
        return $this->Render('abonnement_troismois/detail-user.html.twig', [
            'user'=>$user,
            'medias'=>$medias,
            'langues'=>$langues,
            'loisirs'=>$loisirs,
            'sports'=>$sports,
            'photos'=>$photos,
            'upays'=>$upays,
            'cadeaux'=>$cadeaux,
            'like'=>$like
        ]);

    }

    /**
     * @Route ("/like-to-you", name="like_to_you" )
     */
    public function liketo(Request $request, PaginatorInterface $paginator){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
       $liked = $em->getRepository(Liked::class)->findBy(array('liked'=>$utilisateur));


         $likes= $paginator->paginate($liked, $request->query->getInt('page', 1), 1);
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $medias = $em->getRepository(UserMedia::class)->findBy(array('defaultImg'=>1));
        return $this->render('abonnement_troismois/like-to-you.html.twig',[
            'likes'=>$likes,
            'photos'=>$photos,
            'medias'=>$medias
        ]);

    }


    /**
     * @Route ("/{id}/dislike-to", name="dislike_to" )
     */
    public function disliketo(Request $request, User $id){

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $like= $em ->getRepository(Liked::class)->findOneBy(array('liked'=>$utilisateur, 'utilisateur'=>$userlike));
        $like->setStatus(0);
        $like->setDatecreation(new \DateTime());
        $em->persist($like);
        $em->flush();
        return $this->redirectToRoute('userdetail_to',array('id'=>$userlike->getId()));

    }


    /**
     * @Route ("/{id}/like-to", name="like_to" )
     */
    public function likedto(Request $request, User $id){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $like= $em ->getRepository(Liked::class)->findOneBy(array('liked'=>$utilisateur, 'utilisateur'=>$userlike));
        $like->setStatus(1);
        $like->setDatecreation(new \DateTime());
        $em->persist($like);
        $em->flush();
        return $this->redirectToRoute('userdetail_to',array('id'=>$userlike->getId()));

    }


    /**
     * @Route("/{id}/userdetail-to", name="userdetail_to" )
     */
    public function detailto(Request $request,User $id){
        $em= $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $user= $em->getRepository(FrontUser::class)->findOneBy(array( 'id'=>$id));
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$user));
        $langues = $em->getRepository(LanguesUser::class)->findBy(array('user'=>$utilisateur));
        $loisirs = $em->getRepository(LoisirUser::class)->findBy(array('user'=>$utilisateur));
        $sports = $em->getRepository(SportUser::class)->findBy(array('user'=>$utilisateur));
        $upays = $em->getRepository(PaysUser::class)->findBy(array('user'=>$utilisateur));
        $cadeaux = $em->getRepository(Cadeaux::class)->findAll();
        $medias=$em->getRepository(UserMedia::class)->findBy(['user'=>$user,'status'=>1]);
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $like= $em ->getRepository(Liked::class)->findOneBy(array('liked'=>$utilisateur, 'utilisateur'=>$userlike));

        return $this->Render('abonnement_troismois/user-detail-to.html.twig', [
            'user'=>$user,
            'medias'=>$medias,
            'langues'=>$langues,
            'loisirs'=>$loisirs,
            'sports'=>$sports,
            'photos'=>$photos,
            'upays'=>$upays,
            'cadeaux'=>$cadeaux,
            'like'=>$like
        ]);

    }

    /**
     * @Route ("/{id}/cadeau", name="cadeau" )
     */
    public function cadeau(Request $request, User $id){
       // $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $cadeau= $em ->getRepository(Cadeaux::class)->findOneBy(array('id'=>$request->get('cadeau')));
        $user= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $usercadeau= new CadeauxUser();
        $usercadeau->setUser($user);
        $usercadeau->setCadeau($cadeau);
        $usercadeau->setType(2);
        $usercadeau->setDatecreation(new \DateTime());
        $em->persist($usercadeau);
        $em->flush();

        return $this->redirectToRoute('rencontres');

    }

    /**
     * @Route ("/credit", name="credit" )
     */
    public function credit(Request $request){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $cadeaux = $em->getRepository(CadeauxUser::class)->findBy(array('user'=>$utilisateur));

        return $this->Render('abonnement_troismois/credit.html.twig', [
            'cadeaux'=>$cadeaux
        ]);

    }


    /**
     * @Route ("/{id}/bloqed-to-you", name="bloq_to" )
     */
    public function bloqto(Request $request, User $id){
      $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $like= $em ->getRepository(Liked::class)->findOneBy(array('liked'=>$utilisateur, 'utilisateur'=>$userlike));
        // dd($like);
        $like->setStatus(2);
        $like->setDatecreation(new \DateTime());
        $em->persist($like);
        $em->flush();
        return $this->redirectToRoute('like_to_you');

    }

    /**
     * @Route ("/{id}/bloqed-from-you", name="bloq_from" )
     */
    public function bloqfrom(Request $request, User $id){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $userlike= $em ->getRepository(User::class)->findOneBy(array('id'=>$id));
        $like= $em ->getRepository(Liked::class)->findOneBy(array('utilisateur'=>$utilisateur, 'liked'=>$userlike));
        $like->setStatus(2);
        $like->setDatecreation(new \DateTime());
        $em->persist($like);
        $em->flush();
        return $this->redirectToRoute('like_from_you');

    }
}
