<?php
// src/Controller/LuckyController.php
namespace App\Controller;
use App\Entity\Actualite;
use App\Entity\Caractere;
use App\Entity\Cheveux;
use App\Entity\Commentaire;
use App\Entity\Frontend\User as FrontUser;
use App\Entity\Situation;
use App\Entity\User;
use App\Entity\UserMedia;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LuckyController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function index(){
        $em = $this->getDoctrine()->getManager();
        $situation = $em->getRepository("AppFront:Situation")->findby(["status" => 1]);
        $commentaires= $em->getRepository(Commentaire::class)->findAll();
        $actualites = $em->getRepository(Actualite::class)->findBy(array(),['dateCreation' => 'DESC']);
        $langues = $em->getRepository("AppFront:Langues")->findby(["status" => 1]);
        $caractere = $em->getRepository("AppFront:Caractere")->findby(["status" => 1]);
        $religion= $em->getRepository("AppFront:Religion")->findby(["status" => 1]);
        $pays = $em->getRepository("AppFront:Pays")->findby(["status" => 1]);
        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM App:User u 
                WHERE u.roles NOT LIKE :role
                ORDER BY u.id DESC
                 ')->setParameter('role', '%"ROLE_ADMIN"%');
        $users = $query->getResult();
       // dd($actualites);
        $medias=$em->getRepository(\App\Entity\Frontend\UserMedia::class)->findBy(['defaultImg'=>1]);
        return $this->Render('index.html.twig', [
            'msg'=>0,
            'pays'=>$pays,
            'religion'=>$religion,
            'langues'=>$langues,
            'caractere'=>$caractere,
            'situation'=>$situation,
            'users'=>$users,
            'medias'=>$medias,
            'actualites'=>$actualites,
            'commentaires'=>$commentaires
        ]);
    }
    /**
     * @Route("/actualites", name="actualites")
     */
    public function actualites(Request $request, PaginatorInterface $paginator)
    {
        $em = $this->getDoctrine()->getManager();

        $actualite = $em->getRepository(Actualite::class)->findBy(array(),['dateCreation' => 'DESC']);
        $actualites= $paginator->paginate($actualite, $request->query->getInt('page', 1), 6);
        $commentaires=$em->getRepository(Commentaire::class)->findAll();
        dd($commentaires);
        return $this->Render('actualites.html.twig', [
            'actualites'=>$actualites,

        ]);
    }

    /**
     * @Route("/{id}/actualite", name="actualite_detail")
     */
    public function actualite(Actualite $id)
    {
        $em = $this->getDoctrine()->getManager();

        $actualite = $em->getRepository(Actualite::class)->findOneBy(array('id'=>$id));
        $commentaires= $em->getRepository(Commentaire::class)->findBy(array('actualite'=>$em));
        //dd($commentaires);
        return $this->Render('actualite.html.twig', [
        'actualite'=>$actualite,
            'commentaires'=>$commentaires
        ]);
    }


    /**
     * @Route("/{id}/comment", name="actualite_commentaire")
     */
    public function comment(Actualite $id,Request $request)
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $actualite = $em->getRepository(Actualite::class)->findOneBy(array('id'=>$id));

        $comment = new Commentaire();
        $comment->setUser($utilisateur);
        $comment->setActualite($actualite);
        $comment->setStatus(true);
        $comment->setMessage($request->get('commentaire'));
        $comment->setDateCreation(new \DateTime());
        $em->persist($comment);
        $em->flush();

        return $this->redirectToRoute('actualite_detail', array('id'=>$actualite->getId()));
    }


    /**
    * @Route("/news/{slug}")
    */
    public function number($slug)
    {
        $number = random_int(0, 100);
        return $this->Render('article/article.html.twig', [
            'title' =>   ucwords(str_replace('-', ' ', $slug)),          
        ]);
    }

    /**
     * @Route("/message", name="message")
     */
    public function message( \Swift_Mailer $mailer,Request $request)
    {
        try {
            $message = (new \Swift_Message('contact'))
                ->setFrom('contact.serrurerie2019@gmail.com')
                ->setTo('salwawebdev@gmail.com')
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        'email.html.twig',
                        [
                            'name' => $request->get('name'),
                            'email' => $request->get('mail'),
                            'message' => $request->get('message')
                        ]
                    ),
                    'text/html'
                )
            ;
           //dd($message);

            $mailer->send($message);
            $msg=1;
            //dd($msg);
            return $this->redirectToRoute('homepage', array(
                'msg'=>$msg
            ));


        } catch (FormSizeFileException | IniSizeFileException $e) {
            $response = json_encode(array('result' => $e->getMessage() ));
            return new Response($response,200, array(
                'Content-Type' => 'application/json'
            ));
        }
    }

}