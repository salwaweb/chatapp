<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 07/08/2019
 * Time: 15:58
 */

namespace App\Controller;

use App\Entity\Frontend\Pays;
use App\Entity\Frontend\PaysUser;
use App\Entity\Frontend\UserMedia;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use App\Entity\Frontend\User as FrontUser;
use App\Entity\Frontend\Cheveux as FrontCheveux;
use App\Entity\User;
use App\Entity\LanguesUser;
use App\Entity\LoisirUser;
use App\Entity\SportUser;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class UserController extends AbstractController {

    /**
     * @Route("/sign-in", name="user_login")
     */

    public function login(){

        return $this->Render('user/login.html.twig', [
        ]);
    }

    /**
     * @Route("/sign-up", name="user_registration")
     */

    public function registration(){
        $em = $this->getDoctrine()->getManager();

        $gender = $em->getRepository("AppFront:Gender")->findby(["status" => 1]);

        return $this->Render('user/registration.html.twig', [
            "gender" => $gender,
        ]);
    }

    /**
     * @Route("/register_api", name="user_registration_ajax")
     */

    public function RegistrationForm(Request $request ,UserManagerInterface $userManager, UserPasswordEncoderInterface $passwordEncoder){


        $em = $this->getDoctrine()->getManager();

        $checkmail = $em->getRepository("App:User")->findby(["email" => $request->get("email")]);

        $checkpseudo = $em->getRepository("App:User")->findby(["username" => $request->get("pseudo")]);

        if($checkpseudo != null ){
            return new JsonResponse(['result' => "pseudo exist"]);
        }

        if($checkmail != null ){
            return new JsonResponse(['result' => "mail exist"]);
        }

        $user = $userManager->createUser();
        $user->setUsername($request->get("pseudo"));
        $user->setEmail($request->get("email"));
        $user->setEmailCanonical($request->get("email"));
        $user->addRole("ROLE_USER");

        $user->setEnabled(1); // enable the user or enable it later with a confirmation token in the email
        // this method will encrypt the password with the default settings :)
        $user->setPlainPassword("admin");

       /* $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();

        $serializer = new Serializer([$normalizer], [$encoder]);

        $jsonContent = $serializer->serialize($user, 'json');
        return new JsonResponse(['data' => "success", "user" => json_decode($jsonContent)]);*/

        $userManager->updateUser($user);

        $fos = $em->getRepository(FrontUser::class)->find($user->getId());

        $gender = $em->getRepository("AppFront:Gender")->find($request->get("gender"));

        $fos->setNom($request->get("Firstname"));
        $fos->setPrenom($request->get("Lastname"));
        $fos->setGender($gender);

        $em->persist($fos);

        $em->flush();

        $url = $this->generateUrl(
            'inscription_steps',[
                "email" => $user->getId(),
                ]
        );

        return new JsonResponse(['result' => "success", "urlto" => $url]);


        /*$user = new FOS();
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $request->get('plainPassword')->getData()
            )
        );

        $em->persist($user);
        $em->flush();*/

    }

    /**
     * @Route("/personality/{email}", name="inscription_steps")
     */
    public function InscriptionSteps(Request $request){
        $em = $this->getDoctrine()->getManager();

        $email = $request->get("email");
        $gender = $em->getRepository("AppFront:Gender")->findby(["status" => 1]);
        $alcool = $em->getRepository("AppFront:Alcool")->findby(["status" => 1]);
        $caractere = $em->getRepository("AppFront:Caractere")->findby(["status" => 1]);
        $cheveux = $em->getRepository("AppFront:Cheveux")->findby(["status" => 1]);
        $fumer = $em->getRepository("AppFront:Fumer")->findby(["status" => 1]);
        $langues = $em->getRepository("AppFront:Langues")->findby(["status" => 1]);
        $loisir = $em->getRepository("AppFront:Loisir")->findby(["status" => 1]);
        $pays = $em->getRepository("AppFront:Pays")->findby(["status" => 1]);
        $profession = $em->getRepository("AppFront:Profession")->findby(["status" => 1]);
        $religion = $em->getRepository("AppFront:Religion")->findby(["status" => 1]);
        $situation = $em->getRepository("AppFront:Situation")->findby(["status" => 1]);
        $sport = $em->getRepository("AppFront:Sport")->findby(["status" => 1]);
        $etude = $em->getRepository("AppFront:Etude")->findby(["status" => 1]);
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);
        $jsonContentsport = $serializer->serialize($sport, 'json');
        $jsonContentpays = $serializer->serialize($pays, 'json');
        $jsonContentloisir = $serializer->serialize($loisir, 'json');
        $jsonContentlang = $serializer->serialize($langues, 'json');
        //return new JsonResponse(['data' => "success", "user" => json_decode($jsonContent)]);

        //dd($jsonContent);
        return $this->render("user/register_steps.html.twig", [
            "email" => $email,
            "gender" => $gender,
            "pays" => $pays,
            "pays_data" => $jsonContentpays,
            "alcool" => $alcool,
            "caractere" => $caractere,
            "cheveux" => $cheveux,
            "fumer" => $fumer,
            "etude" => $etude,
            "profession" => $profession,
            "religion" => $religion,
            "situation" => $situation,
            "sport" => $sport,
            "sport_data" => $jsonContentsport,
            "langues" => $langues,
            "langues_data" => $jsonContentlang,
            "loisir" => $loisir,
            "loisir_data" => $jsonContentloisir,
        ]);
    }

    /**
     * @Route("/personality_api", name="personality_ajax")
     */
    public function personalityAPI(Request $request){

        $em = $this->getDoctrine()->getManager();
        $iduser=$request->get("user_mail");
        $user = $em->getRepository(FrontUser::class)->findOneBy(array('id'=>$iduser));

        $sport = json_decode($request->get("sport"));
        $loisir = json_decode($request->get("loisir"));
        $lang = json_decode($request->get("lang"));
        $pays = json_decode($request->get("pays"));

        $uploaddir = './images/';
        $default = 1;
        foreach($_FILES as $file){
            $filename = md5(time()).basename($file['name']);
            if(move_uploaded_file($file['tmp_name'], $uploaddir .$filename)){
                //insert data base
                $usermedia = new UserMedia();
                $usermedia->setDefaultImg($default);
                $usermedia->setPath($filename);
                $usermedia->setUser($user);
                $usermedia->setDateCreation(new \DateTime());
                $em->persist($usermedia);
                $em->flush();
                $default = 0;
            }
        }

        //return new JsonResponse(["img" => $filename]);
        //dd($user);
        $user->setDescription($request->get('aboutme'));
        $user->setBirthday(new \DateTime($request->get('dateofbirth')));
        $user->setTaille($request->get('taille'));
        $user->setPoids($request->get('poid'));
        $user->setAdress($request->get('adress'));
        $pay=$em->getRepository("AppFront:Pays")->findOneBy(["id"=> $request->get('pay')]);
        $user->setPays($pay);
        $user->setVille($request->get('city'));
        $user->setZipdcode($request->get('zipcode'));
        $cheveux = $em->getRepository("AppFront:Cheveux")->findOneBy(["id" => $request->get('cheveux')]);
        $user->setCheveux($cheveux);
        $alcool = $em->getRepository("AppFront:Alcool")->findOneBy(["id" => $request->get('alcool')]);
        $user->setAlcool($alcool);
        $fumeur = $em->getRepository("AppFront:Fumer")->findOneBy(["id" => $request->get('fumer')]);
        $user->setFumeur($fumeur);
        $profession = $em->getRepository("AppFront:Profession")->findOneBy(["id" => $request->get('profession')]);
        $user->setProfession($profession);
        $religion = $em->getRepository("AppFront:Religion")->findOneBy(["id" => $request->get('religion')]);
        $user->setReligion($religion);
        $situation= $em->getRepository("AppFront:Situation")->findOneBy(["id" => $request->get('situation')]);
        $user->setSituation($situation);
        $etude= $em->getRepository("AppFront:Etude")->findOneBy(["id" => $request->get('etude')]);
        $user->setEtude($etude);
        $caractere= $em->getRepository("AppFront:Caractere")->findOneBy(["id" => $request->get('caractere')]);
        $user->setCaractere($caractere);
        $user->setLookingfor($request->get('lookingfor'));
        $em->persist($user);
        $em->flush();



        foreach($sport as $k=>$v){
            $id = $v->value;
            $sprt=$em->getRepository("AppFront:Sport")->findOneBy(["id"=> $id]);
            $usersport = new \App\Entity\Frontend\SportUser();
            $usersport->setUser($user);
            $usersport->setSport($sprt);
            $usersport->setDateCreation(new \DateTime());
            $em->persist($usersport);
            $em->flush();
        }

        foreach($loisir as $k=>$v){
            $id = $v->value;
            $loisir=$em->getRepository("AppFront:Loisir")->findOneBy(["id"=> $id]);
            $userloisir = new \App\Entity\Frontend\LoisirUser();
            $userloisir->setUser($user);
            $userloisir->setLoisir($loisir);
            $userloisir->setDateCreation(new \DateTime());
            $em->persist($userloisir);
            $em->flush();
        }
        foreach($lang as $k=>$v){
            $id = $v->value;
            $lang=$em->getRepository("AppFront:Langues")->findOneBy(["id"=> $id]);
            $userlang = new \App\Entity\Frontend\LanguesUser();
            $userlang->setUser($user);
            $userlang->setLang($lang);
            $userlang->setDateCreation(new \DateTime());
            $em->persist($userlang);
            $em->flush();
        }

        foreach($pays as $k=>$v){
            $id = $v->value;
            $pays=$em->getRepository("AppFront:Pays")->findOneBy(["id"=> $id]);
            $userpays = new PaysUser();
            $userpays->setUser($user);
            $userpays->setPays($pays);
            $userpays->setDateCreation(new \DateTime());
            $em->persist($userpays);
            $em->flush();
        }
        $response = json_encode(array('result' => "success" ));
        return new Response($response,200, array('Content-Type' => 'application/json'));
    }


    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/profile", name="user_profile")
     */

    public function profile(Request $request){


        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $langues = $em->getRepository(LanguesUser::class)->findBy(array('user'=>$utilisateur));
        $loisirs = $em->getRepository(LoisirUser::class)->findBy(array('user'=>$utilisateur));
        $sports = $em->getRepository(SportUser::class)->findBy(array('user'=>$utilisateur));
        $upays = $em->getRepository(PaysUser::class)->findBy(array('user'=>$utilisateur));
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        return $this->Render('user/profile.html.twig', [
            'langues'=>$langues,
            'loisirs'=>$loisirs,
            'sports'=>$sports,
            'photos'=>$photos,
            'upays'=>$upays,
        ]);

    }

    /**
     * @Route("/editprofile/{id}", name="edit_profile")
     */
    public function editprofile(Request $request){
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $email = $request->get("email");
        $gender = $em->getRepository("AppFront:Gender")->findby(["status" => 1]);
        $alcool = $em->getRepository("AppFront:Alcool")->findby(["status" => 1]);
        $caractere = $em->getRepository("AppFront:Caractere")->findby(["status" => 1]);
        $cheveux = $em->getRepository("AppFront:Cheveux")->findby(["status" => 1]);
        $fumer = $em->getRepository("AppFront:Fumer")->findby(["status" => 1]);
        $langues = $em->getRepository("AppFront:Langues")->findby(["status" => 1]);
        $loisir = $em->getRepository("AppFront:Loisir")->findby(["status" => 1]);
        $pays = $em->getRepository("AppFront:Pays")->findby(["status" => 1]);
        $profession = $em->getRepository("AppFront:Profession")->findby(["status" => 1]);
        $religion = $em->getRepository("AppFront:Religion")->findby(["status" => 1]);
        $situation = $em->getRepository("AppFront:Situation")->findby(["status" => 1]);
        $sport = $em->getRepository("AppFront:Sport")->findby(["status" => 1]);
        $etude = $em->getRepository("AppFront:Etude")->findby(["status" => 1]);

        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);
        $jsonContentsport = $serializer->serialize($sport, 'json');
        $jsonContentpays = $serializer->serialize($pays, 'json');
        $jsonContentloisir = $serializer->serialize($loisir, 'json');
        $jsonContentlang = $serializer->serialize($langues, 'json');
        //return new JsonResponse(['data' => "success", "user" => json_decode($jsonContent)]);

        $languages = $em->getRepository(LanguesUser::class)->findBy(array('user'=>$utilisateur));
        $loisirs = $em->getRepository(LoisirUser::class)->findBy(array('user'=>$utilisateur));
        $sports = $em->getRepository(SportUser::class)->findBy(array('user'=>$utilisateur));
        $upays = $em->getRepository(PaysUser::class)->findBy(array('user'=>$utilisateur));
        //dd($jsonContent);
        return $this->render("user/edit-profile.html.twig", [
            "email" => $email,
            "gender" => $gender,
            "pays" => $pays,
            "pays_data" => $jsonContentpays,
            "alcool" => $alcool,
            "caractere" => $caractere,
            "cheveux" => $cheveux,
            "fumer" => $fumer,
            "etude" => $etude,
            "profession" => $profession,
            "religion" => $religion,
            "situation" => $situation,
            "sport" => $sport,
            "sport_data" => $jsonContentsport,
            "langues" => $langues,
            "langues_data" => $jsonContentlang,
            "loisir" => $loisir,
            "loisir_data" => $jsonContentloisir,
            'languages'=>$languages,
            'loisirs'=>$loisirs,
            'sports'=>$sports,
            'upays'=>$upays,
        ]);
    }

    /**
     * @Route("/profile_api", name="profile_ajax")
     */
    public function profileAPI(Request $request){

        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $iduser=$utilisateur->getId();
        $user = $em->getRepository(FrontUser::class)->findOneBy(array('id'=>$iduser));

        $sport = json_decode($request->get("sport"));
        $loisir = json_decode($request->get("loisir"));
        $lang = json_decode($request->get("lang"));
        $pays = json_decode($request->get("pays"));



        //dd($user);
        $user->setDescription($request->get('aboutme'));
        $user->setBirthday(new \DateTime($request->get('dateofbirth')));
        $user->setTaille($request->get('taille'));
        $user->setPoids($request->get('poid'));
        $user->setAdress($request->get('adress'));
        $pay=$em->getRepository("AppFront:Pays")->findOneBy(["id"=> $request->get('pay')]);
        $user->setPays($pay);

        $user->setVille($request->get('city'));
        $user->setZipdcode($request->get('zipcode'));
        $cheveux = $em->getRepository("AppFront:Cheveux")->findOneBy(["id" => $request->get('cheveux')]);
        $user->setCheveux($cheveux);
        $alcool = $em->getRepository("AppFront:Alcool")->findOneBy(["id" => $request->get('alcool')]);
        $user->setAlcool($alcool);
        $fumeur = $em->getRepository("AppFront:Fumer")->findOneBy(["id" => $request->get('fumer')]);
        $user->setFumeur($fumeur);
        $profession = $em->getRepository("AppFront:Profession")->findOneBy(["id" => $request->get('profession')]);
        $user->setProfession($profession);
        $religion = $em->getRepository("AppFront:Religion")->findOneBy(["id" => $request->get('religion')]);
        $user->setReligion($religion);
        $situation= $em->getRepository("AppFront:Situation")->findOneBy(["id" => $request->get('situation')]);
        $user->setSituation($situation);
        $etude= $em->getRepository("AppFront:Etude")->findOneBy(["id" => $request->get('etude')]);
        $user->setEtude($etude);
        $caractere= $em->getRepository("AppFront:Caractere")->findOneBy(["id" => $request->get('caractere')]);
        $user->setCaractere($caractere);
        $em->persist($user);
        $em->flush();



        foreach($sport as $k=>$v){

            $id = $v->value;
            $sprt=$em->getRepository("AppFront:Sport")->findOneBy(["id"=> $id]);

            $usersport = new \App\Entity\Frontend\SportUser();
            $usersport->setUser($user);
            $usersport->setSport($sprt);
            $usersport->setDateCreation(new \DateTime());
            $em->persist($usersport);
            $em->flush();
        }
        foreach($loisir as $k=>$v){

            $id = $v->value;
            $loisir=$em->getRepository("AppFront:Loisir")->findOneBy(["id"=> $id]);

            $userloisir = new \App\Entity\Frontend\LoisirUser();
            $userloisir->setUser($user);
            $userloisir->setLoisir($loisir);
            $userloisir->setDateCreation(new \DateTime());
            $em->persist($userloisir);
            $em->flush();
        }
        foreach($lang as $k=>$v){

            $id = $v->value;
            $lang=$em->getRepository("AppFront:Langues")->findOneBy(["id"=> $id]);

            $userlang = new \App\Entity\Frontend\LanguesUser();
            $userlang->setUser($user);
            $userlang->setLang($lang);
            $userlang->setDateCreation(new \DateTime());
            $em->persist($userlang);
            $em->flush();
        }

        foreach($pays as $k=>$v){

            $id = $v->value;
            $pays=$em->getRepository("AppFront:Pays")->findOneBy(["id"=> $id]);

            $userpays = new PaysUser();
            $userpays->setUser($user);
            $userpays->setPays($pays);
            $userpays->setDateCreation(new \DateTime());
            $em->persist($userpays);
            $em->flush();
        }

        $response = json_encode(array('result' => "success" ));
        return new Response($response,200, array('Content-Type' => 'application/json'));

    }
    /**
     * @Route("/recherche_avancée", name="recherche-avance")
     */
    public function recherche(){
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(FrontUser::class)->findAll();
//dd($users);
        $photos=$em->getRepository(\App\Entity\UserMedia::class)->findBy(['defaultImg'=>1]);

        return $this->Render('recherche-avancee.html.twig', [
            'users'=>$users,
            'photos'=>$photos

        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/gratuit", name="pack_gratuit")
     */

    public function gratuit(){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id'=>$utilisateur->getId()));

        $user->setDateCreation(new \DateTime());
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('fos_user_security_logout');

    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/unmois", name="pack_unmois")
     */
    public function unmois(){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id'=>$utilisateur->getId()));
        $user->addRole('ROLE_UNMOIS');
        $user->setDateCreation(new \DateTime());
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('fos_user_security_logout');

    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/troismois", name="pack_troismois")
     */
    public function troismois(){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id'=>$utilisateur->getId()));
        $user->addRole('ROLE_TROISMOIS');
        $user->setDateCreation(new \DateTime());
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('fos_user_security_logout');

    }

    /**
     * @Route("/demote", name="demote_unmois")
     */
    public function Demoteunmois(){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id'=>$utilisateur->getId()));
        $user->removeRole('ROLE_UNMOIS');
        $user->setDateCreation(new \DateTime());
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('fos_user_security_login');

    }

    /**
     * @Route("/demote/troismois", name="demote_troismois")
     */
    public function Demotetroismois(){
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(array('id'=>$utilisateur->getId()));
        $user->removeRole('ROLE_TROISMOIS');
        $user->setDateCreation(new \DateTime());
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute('fos_user_security_logout');

    }


} 