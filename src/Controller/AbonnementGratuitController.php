<?php

namespace App\Controller;

use App\Entity\CadeauxUser;
use App\Entity\Frontend\PaysUser;
use App\Entity\Frontend\User as FrontUser;
use App\Entity\Frontend\UserMedia;
use App\Entity\Gender;
use App\Entity\LanguesUser;
use App\Entity\LoisirUser;
use App\Entity\Pays;
use App\Entity\Situation;
use App\Entity\SportUser;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
 * @Route("/abonnement")
 */
class AbonnementGratuitController extends AbstractController
{
    /**
     * @Route("/", name="abonnement_gratuit")
     */
    public function index()
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $langues = $em->getRepository(LanguesUser::class)->findBy(array('user'=>$utilisateur));
        $loisirs = $em->getRepository(LoisirUser::class)->findBy(array('user'=>$utilisateur));
        $sports = $em->getRepository(SportUser::class)->findBy(array('user'=>$utilisateur));
        $upays = $em->getRepository(PaysUser::class)->findBy(array('user'=>$utilisateur));
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $cadeaux = $em->getRepository(CadeauxUser::class)->findBy(array('user'=>$utilisateur));

        return $this->render('abonnement_gratuit/index.html.twig', [
            'controller_name' => 'AbonnementGratuitController',
            'langues'=>$langues,
            'loisirs'=>$loisirs,
            'sports'=>$sports,
            'photos'=>$photos,
            'upays'=>$upays,
            'cadeaux'=>$cadeaux,
        ]);
    }

    /**
     * @Route("/photos", name="gratuit_photos")
     */
    public function photos()
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur, 'status'=>1));

        return $this->render('abonnement_gratuit/photos.html.twig', [


            'photos'=>$photos,

        ]);
    }
    /**
     * @Route("/photo-profile", name="photo_profile")
     */
    public function photoprofile()
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $photo = $em->getRepository(UserMedia::class)->findOneBy(array('user'=>$utilisateur, 'defaultImg'=>1,'status'=>1));

        return $this->render('layout-profile/photo-profile.html.twig', [


            'photo'=>$photo,

        ]);
    }

    /**
     * @Route("{media}/modifphoto", name="edit_photo}")
     */
    public function editphotos(Request $request ,UserMedia $media)
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $iduser=$utilisateur->getId();
        $photos = $em->getRepository(UserMedia::class)->findBy(array('id'=>$media));

        return $this->render('abonnement_gratuit/photos.html.twig', [
         'photos'=>$photos,
        ]);
    }

    /**
     * @Route("/{media}/bloqphoto", name="bloq_photo")
     */
    public function bloqphotos(Request $request ,UserMedia $media)
    {

        $em = $this->getDoctrine()->getManager();
        $photos = $em->getRepository(UserMedia::class)->findOneBy(array('id'=>$media));
        $photos->setStatus(0);
        $em->persist($photos);
        $em->flush();
        return $this->redirectToRoute('gratuit_photos');
    }

    /**
     * @Route("/compte", name="gratuit_compte")
     */
    public function compte(Request $request )
    {
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();


        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        return $this->render('abonnement_gratuit/compte.html.twig', [
            'photos'=>$photos,
        ]);
    }

    /**
     * @Route("modifier-compte", name="modif_compte")
     */
    public function modifcompte(Request $request )
    {
        $psw=$request->get('password');

        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $utilisateur->setNom($request->get('nom'));
        $utilisateur->setPrenom($request->get('prenom'));
        $utilisateur->setUsername($request->get('username'));
        $utilisateur->setPlainPassword('admin');

        $em->persist($utilisateur);
        $em->flush();
        return $this->redirectToRoute('abonnement_gratuit');
    }


    /**
     * @Route("/parcourir", name="parcourir_users")
     */
    public function parcourir(Request $request, PaginatorInterface $paginator){
        $em = $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();

        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM App:User u WHERE u.roles NOT LIKE :role'
            )->setParameter('role', '%"ROLE_ADMIN"%');
        $pagination = $query->getResult();
        $users= $paginator->paginate($pagination, $request->query->getInt('page', 1), 4);


        //dd($users);
        $genres = $em->getRepository(Gender::class)->findby(["status" => 1]);
        $situations = $em->getRepository(Situation::class)->findby(["status" => 1]);
        $pays = $em->getRepository(Pays::class)->findby(["status" => 1]);
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $medias=$em->getRepository(UserMedia::class)->findBy(['defaultImg'=>1]);
        //dd($users);
        return $this->Render('abonnement_gratuit/parcourir.html.twig', [
            'users'=>$users,
            'medias'=>$medias,
            'photos'=>$photos,
            'pays'=>$pays,
            'genres'=>$genres,
        'pagination'=>$pagination,
        'situations'=>$situations
        ]);
    }

    /**
     * @Route("/{id}/Detail", name="detail_user" )
     */
    public function detail(Request $request,User $id){
        $em= $this->getDoctrine()->getManager();
        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $photos = $em->getRepository(UserMedia::class)->findBy(array('user'=>$utilisateur));
        $user= $em->getRepository(FrontUser::class)->findOneBy(array( 'id'=>$id));
        //dd($user);
        $medias=$em->getRepository(UserMedia::class)->findBy(['user'=>$user,'status'=>1]);
        return $this->Render('abonnement_gratuit/datailuser.html.twig', [
            'user'=>$user,
            'medias'=>$medias,
            'photos'=>$photos

        ]);

    }
    /**
     * @Route("/test", name="test" )
     */
    public function test(){
        return new Response('<html><body>Admin page!</body></html>');

    }
}
