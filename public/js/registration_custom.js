$(document).ready(function() {
    console.log("registation ready");
    $(document).on('submit','#register_form',function(e){
        e.preventDefault();
        // code

        console.log("form submit");
        var $pass = $("input[name=Password]");
        var $passConfirm = $("input[name=PasswordConfirm]");
        var $Firstname = $("input[name=Firstname]");
        var $Lastname = $("input[name=Lastname]");
        var $pseudo = $("input[name=pseudo]");
        var $email = $("input[name=email]");

        $passConfirm.removeClass("input-error");
        console.log($pass.val(), $passConfirm.val());
        if($pass.val() != $passConfirm.val()){
            //$passConfirm.removeClass("input-success");
            $passConfirm.addClass("input-error");

            return false;
        }

        var form = $(this);
        var url = form.attr('action');


        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                console.log(data); // show response from the php script.

                if(data.result === "success"){
                    location.href = data.urlto;
                }
            }
        });


    });

});
